package org.upiita;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class scraping {
    static List<Pizza> listPizzaInfo;
    static List<WebElement> listPizzaRecopiled;
    static Pizza element;
    static WebDriver driver;
    static String chromePath = System.getProperty("user.dir") + "\\drivers\\chromedriver.exe";


    public static String findPizza(String product) {
        String result = "{\"Pizzas\": { ";
        listPizzaInfo = new ArrayList<Pizza>();
        listPizzaRecopiled = new ArrayList<WebElement>();
        String baseURL;
        String xPath;
        int count = 0;
        System.setProperty("webdriver.chrome.driver", chromePath);
        driver = new ChromeDriver();
        //Little Caesar
        baseURL = "https://www.ubereats.com/mx/mexico-city/food-delivery/little-caesars-montevideo/G87YejRrQrqWO49hww5fHQ";
        xPath = "//*[@id=\"wrapper\"]/main/div[2]/ul/li[4]/ul/li";
        scrapingPizza(baseURL, xPath, "Little Caesars");

        //Pizza Hut
        baseURL = "https://www.ubereats.com/mx/mexico-city/food-delivery/pizza-hut-plaza-torres-lindavista/ApRlA4hTSuKYsDEHGAE00A";
        xPath = "//*[@id=\"wrapper\"]/main/div[2]/ul/li[4]/ul/li";
        scrapingPizza(baseURL, xPath, "Pizza Hut");

        //Papa John's
        baseURL = "https://www.ubereats.com/mx/mexico-city/food-delivery/papa-johns-pizza-lindavista/miEwW3wOR-iM3IFrvlRLTQ";
        xPath = "//*[@id=\"wrapper\"]/main/div[2]/ul/li[8]/ul/li";
        scrapingPizza(baseURL, xPath, "Papa John's");

        driver.quit();
        for (Pizza p : listPizzaInfo) {
            if (p.type.toLowerCase().contains(product) || p.description.toLowerCase().contains(product)) {
                if (count == 0) {
                    result = result + "\"" + p.type + "\":{ \"Marca\": \"" + p.brand + "\", \"Descrición\": \"" + p.description.replace('\"',' ')
                            + "\", \"Precio\": \"" + p.price + "\" }";
                    count++;
                }
                else
                    result = result + ", \"" + p.type + "\":{ \"Marca\": \"" + p.brand + "\", \"Descrición\": \"" + p.description.replace('\"',' ')
                            + "\", \"Precio\": \"" + p.price + "\" }";
            }
        }
        result = result + "} }";
        return result;
    }

    public static void scrapingPizza(String baseURL, String xPath, String brand){
        try {
            driver.get(baseURL);
            listPizzaRecopiled = driver.findElements(By.xpath(xPath));
            for (WebElement dataPizza: listPizzaRecopiled) {
                element = new Pizza(brand, dataPizza);
                listPizzaInfo.add(element);
            }
        }catch (NoSuchElementException ne){
            System.err.println("No se encontró el Web Element: " + ne.getMessage());
        }catch (Exception e){
            System.err.println("Error de java " + e.getMessage());
        }

    }
}
